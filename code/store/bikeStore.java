// Nicholas Poulioezos 2039039
package store;
import bike_states.bicycle;
public class bikeStore{
    public static void main(String[] args){
 
        bicycle [] bike =new bicycle[4];

        bike[0] = new bicycle("Adler", 6, 23.1);

        bike[1] = new bicycle("CCM", 3, 42.5);

        bike[2] = new bicycle("Felt ", 14, 63.9);

        bike[3] = new bicycle("VéloSoleX", 7, 52.1);

        for (bicycle allBike : bike){
            System.out.println(allBike.toString());
        }

    } 

    
}