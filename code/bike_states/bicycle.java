// nicholas Pouliezos 2039039
package bike_states;
public class bicycle {

    private String manufacturer;
    private int numberGears;
    private double maxSpeed;

    public String getManufacturer(){

        return this.manufacturer;
    }

    public int getnumberGears(){

        return this.numberGears;
    }

    public double getmaxSpeed(){

        return this.maxSpeed;
    }

    public bicycle(String manu, int gears, double speed){
        this.manufacturer=manu;
        this.numberGears=gears;
        this.maxSpeed = speed;
    }

    public String toString(){
        return "Manufacturer: "+ this.manufacturer +", Number of Gears: " +numberGears + ", MaxSpeed: " + maxSpeed;
        
    }

}